var token1 = window.localStorage.getItem("token1");
var token2 = window.localStorage.getItem("token2");
var username = window.localStorage.getItem("username");
var banji = window.localStorage.getItem("banji");
var name = window.localStorage.getItem("name");
var nickname = window.localStorage.getItem("nickname");
var domain = 'https://www.caizhenhang.cn/';
//var domain = 'http://192.168.199.217:8080/';
var login = isLogin();

function isLogin() {
	if(token1 != null && token2 != null) {
		return true;
	} else {
		return false;
	}
}


function removeAllStorage() {
	window.localStorage.removeItem('token1');
	window.localStorage.removeItem('token2');
	window.localStorage.removeItem('username');
	window.localStorage.removeItem('banji');
	window.localStorage.removeItem('name');
	window.localStorage.removeItem('nickname');
}

function getApi(name) {
	return domain + name;
}

function replaceHistory(url) {
	var state = {
		title: null,
		url: url
	};
	window.history.replaceState(state,null,url);
}

function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r != null) return unescape(r[2]);
	return null;
}