var ajaxUrl , update;

mui.init({
	pullRefresh: {
		container: '#pullrefresh',
		down: {
			auto: true,
			style: 'circle',
			callback: pulldownRefresh
		},
		up: {
			auto: false,
			contentrefresh: '正在加载...',
			callback: pullupRefresh
		}
	}
});

var offset = 0;
mui('.mui-scroll-wrapper').scroll();

function Scroll(url, add) {
	ajaxUrl = url;
	update = add;
}

function pullupRefresh() {
	mui.ajax(ajaxUrl, {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
		},
		data: {
			limit: 20,
			offset: offset
		},
		dataType: 'json',
		type: 'post',
		timeout: 10000,
		success: function(data) {
			update(data);
			offset += data.array.length;
			mui('#pullrefresh').pullRefresh().endPullupToRefresh((data.array.length != 20));
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('加载失败');
			mui('#pullrefresh').pullRefresh().endPullupToRefresh(true);
		}
	});
}

function pulldownRefresh() {
	mui.ajax(ajaxUrl, {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
		},
		data: {
			limit: 20,
			offset: 0
		},
		dataType: 'json',
		type: 'post',
		timeout: 10000,
		success: function(data) {
			document.body.querySelector('.mui-scroll').innerHTML = '';
			update(data);
			offset += data.array.length;
			mui('#pullrefresh').pullRefresh().endPulldownToRefresh();
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('加载失败');
			mui('#pullrefresh').pullRefresh().endPulldownToRefresh();
		}
	});
}